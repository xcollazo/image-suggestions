#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math

from conftest import assert_shallow_equals
from image_suggestions import commonswiki_file, shared


def test_get_commonswiki_file_data(spark_session):
    wd_data = spark_session.createDataFrame(
        [
            # a) regular p18 tag
            (20, 'p18', 'Q999', 19),
            # b) 2 p373 tags for the same page_id, with values in reverse order
            (20, 'p373', 'Q888', 1000),
            (20, 'p373', 'Q777', 0),
        ],
        ['page_id', 'tag', 'item_id', 'score']
    )
    li_data = spark_session.createDataFrame(
        [
            # c) regular lead_image tag
            (20, 'Q999', 'lead_image', 19, ['arwiki','gawiki']),
            # d) many lead_image tags for the same page_id, with values out of order
            (10, 'Q888', 'lead_image', 1000, ['enwiki']),
            (10, 'Q666', 'lead_image', 6, ['itwiki','gawiki']),
            (10, 'Q999', 'lead_image', 200, ['gawiki']),
            (10, 'Q777', 'lead_image', 11, ['itwiki']),
        ],
        # note the column order in wd_data and li_data is different, to make sure the code is robust enough
        # to deal with it
        ['page_id', 'item_id', 'tag', 'score', 'found_on']
    )
    ddf = commonswiki_file.get_commonswiki_file_data(wd_data, li_data)
    expected = spark_session.createDataFrame(
        [
            # d) checking order by page_id and tag, and ordering of values
            ('commonswiki', 6, 10, 'lead_image', ['Q666|6', 'Q777|11', 'Q888|1000', 'Q999|200']),
            # c)
            ('commonswiki', 6, 20, 'lead_image', ['Q999|19']),
            # a)
            ('commonswiki', 6, 20, 'p18', ['Q999|19']),
            # b)
            ('commonswiki', 6, 20, 'p373', ['Q777|0', 'Q888|1000']),
        ],
        ['wikiid', 'page_namespace', 'page_id', 'tag', 'values']
    )
    assert_shallow_equals(ddf, expected)



# Not a test
def compute_p373_score(pages_in_category):
    return int(
        round(
            (1 / math.log(pages_in_category + 1)) * commonswiki_file.MAX_SCORE / 1.443
        )
    )


def test_gather_categories_with_links(spark_session, mocker):
    mocker.patch(
        'image_suggestions.commonswiki_file.load_categories',
        return_value=spark_session.createDataFrame(
            [
                ('category_1', 111),
                ('category_2', 222),
                ('category_2', 333),
                ('category_2', 444),
                ('category_2', 555)
            ],
            ['cat_title', 'cat_pages'],
        ),
    )
    mocker.patch(
        'image_suggestions.commonswiki_file.load_category_links',
        return_value=spark_session.createDataFrame(
            [
                (77, 'category_0'),
                (88, 'category_1'),
                (99, 'category_2'),
            ],
            ['page_id', 'cat_title']
        ),
    )
    expected = spark_session.createDataFrame(
        [
            ('category_2', 333, 99),
            ('category_2', 222, 99),
            ('category_2', 444, 99),
            ('category_2', 555, 99),
            ('category_1', 111, 88),
        ],
        ['cat_title', 'cat_pages', 'page_id'],
    )

    assert_shallow_equals(commonswiki_file.gather_categories_with_links('short'), expected)


def test_gather_commons_with_reverse_p18(spark_session):
    commons_file_pages = spark_session.createDataFrame(
        [
            (111, 'commons_page_1'),
            (222, 'commons_page_2'),
            (333, 'commons_page_3'),
        ],
        ['page_id', 'page_title']
    )
    wikidata_items_with_P18 = spark_session.createDataFrame(
        [
            ('Q555', 'commons_page_99'),
            ('Q666', 'commons_page_1'),
            ('Q777', 'commons_page_1'),
            ('Q888', 'commons_page_2'),
        ],
        ['item_id', 'value'],
    )
    expected = spark_session.createDataFrame(
        [
            (111, 'Q666', commonswiki_file.P18_TAG, commonswiki_file.MAX_SCORE),
            (111, 'Q777', commonswiki_file.P18_TAG, commonswiki_file.MAX_SCORE),
            (222, 'Q888', commonswiki_file.P18_TAG, commonswiki_file.MAX_SCORE),
        ],
        ['page_id', 'item_id', shared.TAG_COLNAME, shared.SCORE_COLNAME],
    )

    assert_shallow_equals(
        commonswiki_file.gather_commons_with_reverse_p18(
            commons_file_pages, wikidata_items_with_P18
        ),
        expected
    )


def test_gather_all_commons_with_reverse_P373(spark_session, mocker):
    mocker.patch(
        'image_suggestions.commonswiki_file.gather_categories_with_links',
        return_value=spark_session.createDataFrame(
            [
                ('category_a', 2, 111),
                ('category_b', 20, 111),
                ('category_b', 20, 222),
                ('category_b', 20, 333),
                ('category_linked_to_non_existent_file', 30, 777),
                ('category_not_linked_via_P373', 30, 888),
            ],
            ['cat_title', 'cat_pages', 'page_id'],
        ),
    )
    commons_file_pages = spark_session.createDataFrame(
        [
            (111, 'commons_page_1'),
            (222, 'commons_page_2'),
            (333, 'commons_page_3'),
            (888, 'commons_page_in_category_not_linked_via_P373'),
            (999, 'commons_page_not_in_category'),
        ],
        ['page_id', 'page_title'],
    )
    wikidata_items_with_P373 = spark_session.createDataFrame(
        [
            ('Q1', 'category_a'),
            ('Q2', 'category_b'),
            ('Q3', 'category_c'),
            ('Q4', 'non_existent_category'),
            ('Q5', 'category_linked_to_non_existent_file'),
        ],
        ['item_id', 'value'],
    )
    expected = spark_session.createDataFrame(
        [
            (111, 'Q1', commonswiki_file.P373_TAG, compute_p373_score(2)),
            (111, 'Q2', commonswiki_file.P373_TAG, compute_p373_score(20)),
            (222, 'Q2', commonswiki_file.P373_TAG, compute_p373_score(20)),
            (333, 'Q2', commonswiki_file.P373_TAG, compute_p373_score(20)),
        ],
        ['page_id', 'item_id', shared.TAG_COLNAME, shared.SCORE_COLNAME],
    )

    assert_shallow_equals(
        commonswiki_file.gather_all_commons_with_reverse_p373(commons_file_pages, wikidata_items_with_P373, 'short'),
        expected
    )


def test_gather_wikidata_data(spark_session, mocker):
    commons_file_pages = spark_session.createDataFrame(
        [(111, 'commons_page_1')],
        ['page_id', 'page_title']
    )
    wikidata_items_with_P18 = spark_session.createDataFrame(
        [('Q1', 'commons_page_1')],
        ['item_id', 'value']
    )
    wikidata_items_with_P373 = spark_session.createDataFrame(
        [('Q2', 'category')],
        ['item_id', 'value']
    )
    hive_db = 'hive'
    coalesce = 2

    P18 = spark_session.createDataFrame(
        [(111, 'Q666', commonswiki_file.P18_TAG, commonswiki_file.MAX_SCORE)],
        ['page_id', 'item_id', shared.TAG_COLNAME, shared.SCORE_COLNAME],
    )
    mocker.patch('image_suggestions.commonswiki_file.gather_commons_with_reverse_p18', return_value=P18)
    P373 = spark_session.createDataFrame(
        [(111, 'Q1', commonswiki_file.P373_TAG, 5), (222, 'Q2', commonswiki_file.P373_TAG, 55)],
        ['page_id', 'item_id', shared.TAG_COLNAME, shared.SCORE_COLNAME],
    )
    mocker.patch('image_suggestions.commonswiki_file.gather_commons_with_reverse_p373', return_value=P373)
    expected = spark_session.createDataFrame(
        [
            (111, 'Q666', commonswiki_file.P18_TAG, commonswiki_file.MAX_SCORE),
            (111, 'Q1', commonswiki_file.P373_TAG, 5),
            (222, 'Q2', commonswiki_file.P373_TAG, 55),
        ],
        ['page_id', 'item_id', shared.TAG_COLNAME, shared.SCORE_COLNAME],
    )
    mocker.patch('image_suggestions.commonswiki_file.save_wikidata_data', return_value=expected)

    assert_shallow_equals(
        commonswiki_file.gather_wikidata_data(
            commons_file_pages, wikidata_items_with_P18, wikidata_items_with_P373, '2022-05-16', hive_db, coalesce
        ),
        expected,
    )

    # test functions were called correctly
    commonswiki_file.gather_commons_with_reverse_p18.assert_called_with(
        commons_file_pages, wikidata_items_with_P18
    )
    commonswiki_file.gather_commons_with_reverse_p373.assert_called_with(
        commons_file_pages, wikidata_items_with_P373, '2022-04'
    )
    commonswiki_file.save_wikidata_data.assert_called()


def test_gather_all_articles_with_lead_images(spark_session, mocker):
    mocker.patch(
        'image_suggestions.commonswiki_file.load_commons_file_pages',
        return_value=spark_session.createDataFrame(
            [
                (1, 'commons_1'),
                (2, 'commons_2'),
                (3, 'commons_file_not_used_as_lead_image'),
            ],
            ['page_id', 'page_title']
        ),
    )
    mocker.patch(
        'image_suggestions.commonswiki_file.load_lead_images',
        return_value=spark_session.createDataFrame(
            [
                ('ptwiki', 10, 'commons_1'),
                ('ruwiki', 20, 'commons_2'),
                ('ptwiki', 30, 'nonexistent_commons_page'),
            ],
            ['wiki_db', 'page_id', 'lead_image_title']
        ),
    )
    mocker.patch(
        'image_suggestions.commonswiki_file.load_non_commons_main_pages',
        return_value=spark_session.createDataFrame(
            [
                ('ptwiki', 10, 'article_1'),
                ('ruwiki', 20, 'article_2'),
                ('ptwiki', 30, 'article_with_broken_lead_image_link'),
                ('gawiki', 40, 'article_with_no_lead_image'),
            ],
            ['wiki_db', 'page_id', 'page_title']
        ),
    )
    mocker.patch(
        'image_suggestions.commonswiki_file.load_wikidata_item_page_links',
        return_value=spark_session.createDataFrame(
            [
                ('ptwiki', 10, 'Q100'),
                ('ruwiki', 20, 'Q101'),
            ],
            ['wiki_db', 'page_id', 'item_id']
        ),
    )
    expected = spark_session.createDataFrame(
        [
            ('ptwiki', 'article_1', 'Q100', 1),
            ('ruwiki', 'article_2', 'Q101', 2),
        ],
        ['article_wiki', 'article_title', 'item_id', 'commons_page_id']
    )
    articles_with_lead_images = commonswiki_file.gather_all_articles_with_lead_images('2022-05-16', '2022-04')

    assert_shallow_equals(articles_with_lead_images, expected)


def test_add_link_counts(spark_session, mocker):
    articles_with_lead_images = spark_session.createDataFrame(
        [
            ('ptwiki', 'article_1', 'Q100', 1),
            ('ruwiki', 'article_2', 'Q101', 2),
            ('gawiki', 'article_3', 'Q101', 2),
        ],
        ['article_wiki', 'article_title', 'item_id', 'commons_page_id']
    )
    mocker.patch(
        'image_suggestions.commonswiki_file.load_pagelinks',
        return_value=spark_session.createDataFrame(
            [
                ('ptwiki', 'article_1', 1),
                ('ptwiki', 'article_1', 2),
                ('ptwiki', 'article_1', 3),
                ('ptwiki', 'article_1', 4),
                ('ruwiki', 'article_2', 11),
                ('ruwiki', 'article_2', 12),
                ('gawiki', 'article_3', 21),
            ],
            ['wiki_db', 'to_title', 'from_id']
        ),
    )
    with_lead_images = commonswiki_file.add_link_counts('short', articles_with_lead_images)

    expected = spark_session.createDataFrame(
        [
            (1, 'Q100', 4, ['ptwiki']),
            (2, 'Q101', 3, ['gawiki', 'ruwiki']),
        ],
        ['commons_page_id', 'item_id', 'incoming_link_count_all', 'found_on']
    )

    assert_shallow_equals(with_lead_images, expected)


def test_gather_lead_image_data(spark_session, mocker):
    hive_db = 'hive'
    snapshot = '2022-05-16'
    coalesce = 2
    mocker.patch('image_suggestions.commonswiki_file.gather_articles_with_lead_images')
    mocker.patch(
        'image_suggestions.commonswiki_file.add_link_counts',
        return_value=spark_session.createDataFrame(
            [
                ('Q100', 1, 10000, ['enwiki', 'itwiki']),  # check max score is 1000
                ('Q101', 2, 1, ['frwiki']),  # check min score if there are ANY links is 1
                ('Q808', 3, 500, ['arwiki']),
            ],
            ['item_id', 'commons_page_id', 'incoming_link_count_all', shared.FOUND_ON_COLNAME]
        ),
    )
    expected = spark_session.createDataFrame(
        [
            (1, 'Q100', commonswiki_file.LEAD_IMAGE_TAG, 1000, ['enwiki', 'itwiki']),
            (2, 'Q101', commonswiki_file.LEAD_IMAGE_TAG, 1, ['frwiki']),
            (3, 'Q808', commonswiki_file.LEAD_IMAGE_TAG, 100, ['arwiki']),
        ],
        [
            'page_id', 'item_id', shared.TAG_COLNAME,
            shared.SCORE_COLNAME, shared.FOUND_ON_COLNAME
        ]
    )
    mocker.patch('image_suggestions.commonswiki_file.save_lead_image_data', return_value=expected)

    lead_image_data = commonswiki_file.gather_lead_image_data(snapshot, hive_db, coalesce)
    assert_shallow_equals( lead_image_data, expected )
    commonswiki_file.save_lead_image_data.assert_called()
    commonswiki_file.gather_articles_with_lead_images.assert_called()

