#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from conftest import assert_shallow_equals
from image_suggestions import search_indices, shared


def test_get_search_index_data(spark_session, mocker):
    mocker.patch(
        'image_suggestions.search_indices.load_suggestions',
        return_value=spark_session.createDataFrame(
            [
                {"wiki": "aawiki", "page_id": 111 },
                {"wiki": "bbwiki", "page_id": 222 },
                {"wiki": "ccwiki", "page_id": 333 },
            ],
        ),
    )
    expected = spark_session.createDataFrame(
        [
            ('aawiki', 0, 111, 'recommendation.image', ['exists|1']),
            ('bbwiki', 0, 222, 'recommendation.image', ['exists|1']),
            ('ccwiki', 0, 333, 'recommendation.image', ['exists|1']),
        ],
        [shared.WIKIID_COLNAME, shared.PAGE_NAMESPACE_COLNAME, 'page_id', shared.TAG_COLNAME, shared.VALUES_COLNAME],
    )
    actual = search_indices.get_search_index_data('hive_db', '2022-05-17')
    assert_shallow_equals(actual, expected)
    # test functions were called correctly
    search_indices.load_suggestions.assert_called_with('hive_db', '2022-05-17')

