#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from conftest import assert_shallow_equals
from image_suggestions import shared


# Note - this is complicated. See comments on shared.get_monthly_snapshot
def test_get_monthly_snapshot():
    # end of the week in this month, so latest monthly timestamp is last month
    assert shared.get_monthly_snapshot('2022-05-16') == '2022-04'
    assert shared.get_monthly_snapshot('2022-05-01') == '2022-04'
    assert shared.get_monthly_snapshot('2022-01-02') == '2021-12'
    # end of the week in next month, so timestamp is this month
    assert shared.get_monthly_snapshot('2022-05-30') == '2022-05'
    assert shared.get_monthly_snapshot('2021-12-27') == '2021-12'

def test_compute_search_index_delta(spark_session):
    previous = spark_session.createDataFrame(
        [
            # a) not in current, so should be deleted
            ('commonswiki', 6, 111, 'P18', ['Q123|1000']),
            # b) identical in current and previous, so should be absent from delta
            ('commonswiki', 6, 112, 'P373', ['Q123|99', 'Q345|0']),
            # c) present in current
            ('commonswiki', 6, 113, 'P18', ['Q123|1000']),
            # d) different value from current
            ('commonswiki', 6, 212, 'lead_image', ['Q678|88']),
            # e) different page_id compared to current
            ('commonswiki', 6, 215, 'P18', ['Q678|88']),
            # f) different tag compared to current
            ('commonswiki', 6, 300, 'P18', ['Q789|22']),
            # a1) not in current, so should be deleted
            ('xwiki', 0, 111, 'recommendation.image', ['exists|1']),
            # b1) identical in current and previous, so should be absent from delta
            ('xwiki', 0, 112, 'recommendation.image', ['exists|1']),
            # a2) same in current and previous, so should be absent from delta
            ('y1wiki', 0, 555, 'recommendation.image', ['exists|1']),
            ('y2wiki', 0, 555, 'recommendation.image', ['exists|1']),
            ('y3wiki', 0, 555, 'recommendation.image', ['exists|1']),
            # a3) duplicate in previous
            ('zwiki', 0, 555, 'recommendation.image', ['exists|1']),
            ('zwiki', 0, 555, 'recommendation.image', ['exists|1']),
        ],
        ['wikiid', 'page_namespace', 'page_id', 'tag', 'values'],
    )
    current = spark_session.createDataFrame(
        [
            # b) identical in current and previous, so should be absent from delta
            ('commonswiki', 6, 112, 'P373', ['Q123|99', 'Q345|0']),
            # c) present in previous ... but also has a new value, so expect a new set containing both values
            ('commonswiki', 6, 113, 'P18', ['Q123|1000', 'Q345|999']),
            # d) different value from previous, should be updated
            ('commonswiki', 6, 212, 'lead_image', ['Q678|87']),
            # e) different page_id compared to previous, old page_id should be deleted and the new one added
            ('commonswiki', 6, 216, 'P18', ['Q678|1000']),
            # f) different tag compared to previous, old tag should be deleted and the new one added
            ('commonswiki', 6, 300, 'P373', ['Q789|22']),
            # g) absent from previous, so expected in delta
            ('commonswiki', 6, 333, 'lead_image', ['Q456|1000', 'Q567|99']),
            # b1) identical in current and previous, so should be absent from delta
            ('xwiki', 0, 112, 'recommendation.image', ['exists|1']),
            # c1) new in current, should be present in delta
            ('xwiki', 0, 113, 'recommendation.image', ['exists|1']),
            # a2) same in current and previous, so should be absent from delta
            ('y1wiki', 0, 555, 'recommendation.image', ['exists|1']),
            ('y2wiki', 0, 555, 'recommendation.image', ['exists|1']),
            ('y3wiki', 0, 555, 'recommendation.image', ['exists|1']),
            # a3) not duplicated in current, still should be absent from delta
            ('zwiki', 0, 555, 'recommendation.image', ['exists|1']),
        ],
        ['wikiid', 'page_namespace', 'page_id', 'tag', 'values'],
    )
    expected = spark_session.createDataFrame(
        [
            # a)
            ('commonswiki', 6, 111, 'P18', ['__DELETE_GROUPING__']),
            # c)
            ('commonswiki', 6, 113, 'P18', ['Q123|1000', 'Q345|999']),
            # d)
            ('commonswiki', 6, 212, 'lead_image', ['Q678|87']),
            # e)
            ('commonswiki', 6, 215, 'P18', ['__DELETE_GROUPING__']),
            ('commonswiki', 6, 216, 'P18', ['Q678|1000']),
            # f)
            ('commonswiki', 6, 300, 'P18', ['__DELETE_GROUPING__']),
            ('commonswiki', 6, 300, 'P373', ['Q789|22']),
            # g)
            ('commonswiki', 6, 333, 'lead_image', ['Q456|1000', 'Q567|99']),
            # a1)
            ('xwiki', 0, 111, 'recommendation.image', ['__DELETE_GROUPING__']),
            # c1)
            ('xwiki', 0, 113, 'recommendation.image', ['exists|1']),
        ],
        ['wikiid', 'page_namespace', 'page_id', 'tag', 'values'],
    )
    actual = shared.compute_search_index_delta(previous, current)
    assert_shallow_equals(actual, expected)

def test_write_search_index_full(spark_session, mocker):
    mocker.patch('image_suggestions.shared.save_table')
    data = spark_session.createDataFrame(
        [
            ('commonswiki', 6, 10, 'lead_image', ['Q666|6', 'Q777|11', 'Q888|1000', 'Q999|200']),
            ('commonswiki', 6, 20, 'lead_image', ['Q999|19']),
        ],
        ['wikiid', 'page_namespace', 'page_id', 'tag', 'values']
    )

    hive_db = 'some_hive_db'
    snapshot = '2022-05-11'
    coalesce = 2

    returned_data = shared.write_search_index_full(data, hive_db, snapshot, coalesce)
    assert_shallow_equals(data, returned_data)
    shared.save_table.assert_called()


def test_write_search_index_delta(spark_session, mocker):
    mocker.patch('image_suggestions.shared.save_table')
    mocker.patch('image_suggestions.shared.get_data_from_previous_snapshot')
    data = spark_session.createDataFrame(
        [
            ('commonswiki', 6, 10, 'lead_image', ['Q666|6', 'Q777|11', 'Q888|1000', 'Q999|200']),
            ('commonswiki', 6, 20, 'lead_image', ['Q999|19']),
        ],
        ['wikiid', 'page_namespace', 'page_id', 'tag', 'values']
    )
    delta = spark_session.createDataFrame(
        [
            ('commonswiki', 6, 20, 'p18', ['Q999|19']),
            ('commonswiki', 6, 20, 'p373', ['Q777|0', 'Q888|1000']),
        ],
        ['wikiid', 'page_namespace', 'page_id', 'tag', 'values']
    )
    mocker.patch(
        'image_suggestions.shared.compute_search_index_delta',
        return_value=delta
    )

    hive_db = 'some_hive_db'
    snapshot = '2022-05-11'
    previous_snapshot = '2022-05-04'
    coalesce = 2

    shared.write_search_index_delta(data, hive_db, snapshot, previous_snapshot, coalesce, spark_session)
    shared.save_table.assert_called()
