#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Gather image suggestions for export to Cassandra"""

import argparse
import uuid

from fsspec import filesystem  # type: ignore
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F
from pyspark.sql.types import NullType

from image_suggestions import queries, shared

# Images with filenames containing these substrings are probably not real illustrations for an article - they're
# probably icons or placeholders
PLACEHOLDER_IMAGE_SUBSTRINGS = (
    'flag',
    'noantimage',
    'no_free_image',
    'image_manquante',
    'replace_this_image',
    'disambig',
    'regions',
    'map',
    'default',
    'defaut',
    'falta_imagem_',
    'imageNA',
    'noimage',
    'noenzyimage',
)

# We have a stored DataFrame containing images that are in placeholder categories
# See https://petscan.wmflabs.org/?psid=18699732&format=json
PLACEHOLDER_IMAGE_PARQUET = 'image_placeholders'

# If an article corresponds to a wikidata item that is an instance of a list, or a year, or a name, etc it is not a
# candidate for illustration. Property id for 'instance of' is P31
UNILLUSTRATABLE_P31 = [
    'Q577',  # year
    'Q29964144',  # calendar year
    'Q14795564',  # recurrent timeframe
    'Q3311614',  # century leap year
    'Q101352',  # family name
    'Q82799',  # name
    'Q4167410',  # disambiguation page
    'Q21199',  # natural number
    'Q28920044',  # positive integer
    'Q28920052',  # non negative integer
    'Q13406463',  # list
    'Q22808320',  # wikimedia human name disambiguation page
]

CONFIDENCE_COLNAME = 'confidence'
KIND_COLNAME = 'kind'
DATASET_ID_COLNAME = 'id'
ORIGIN_WIKI_COLNAME = 'origin_wiki'

SUGGESTIONS_TABLE = 'image_suggestions_suggestions'
TITLE_CACHE_TABLE = 'image_suggestions_title_cache'
INSTANCEOF_CACHE_TABLE = 'image_suggestions_instanceof_cache'


# BEGIN: initial queries against the data lake
# TODO `spark` is an outer-score variable: pass it as an arg to all the functions below
def load_local_images(short_snapshot: str) -> DataFrame:  # pragma: no cover
    return spark.sql(queries.local_images.format(short_snapshot))

def load_commons_images(short_snapshot: str) -> DataFrame:  # pragma: no cover
    return spark.sql(queries.commons_file_pages.format(short_snapshot))


def load_imagelinks(short_snapshot: str) -> DataFrame:  # pragma: no cover
    return spark.sql(queries.imagelinks.format(short_snapshot))


def load_non_commons_main_pages(short_snapshot: str) -> DataFrame:  # pragma: no cover
    return spark.sql(queries.non_commons_main_pages.format(short_snapshot))


def load_wikidata_item_page_links(snapshot: str) -> DataFrame:  # pragma: no cover
    return spark.sql(queries.wikidata_item_page_links.format(snapshot))


def load_wikidata_items_with_P31(snapshot: str) -> DataFrame:  # pragma: no cover
    return spark.sql(queries.wikidata_items_with_P31.format(snapshot))


def load_latest_revisions(short_snapshot: str) -> DataFrame:  # pragma: no cover
    return spark.sql(queries.latest_revisions.format(short_snapshot))


# Get images that are in a placeholder category on commons
# Returns DataFrame with column `page_title`
def load_images_in_placeholder_categories() -> DataFrame:  # pragma: no cover
    return spark.read.parquet(PLACEHOLDER_IMAGE_PARQUET)


def load_wiki_sizes(short_snapshot: str) -> DataFrame:  # pragma: no cover
    return spark.sql(queries.wiki_sizes.format(short_snapshot))


def load_commons_pages_with_depicts(snapshot: str) -> DataFrame:  # pragma: no cover
    return spark.sql(queries.commons_pages_with_depicts.format(snapshot))


def load_lead_image_data_latest(hive_db: str, snapshot: str) -> DataFrame:  # pragma: no cover
    return spark.sql(
        f'SELECT * FROM {hive_db}.{shared.LEAD_IMAGE_DATA} WHERE snapshot="{snapshot}"'
    )


def load_wikidata_data_latest(hive_db: str, snapshot: str) -> DataFrame:  # pragma: no cover
    return spark.sql(
        f'SELECT * FROM {hive_db}.{shared.WIKIDATA_DATA} WHERE snapshot="{snapshot}"'
    )

def get_wiktionaries() -> list: # pragma: no cover
    return [
        'cswiktionary', 'frwiktionary', 'fiwiktionary', 'sqwiktionary', 'yiwiktionary', 'sdwiktionary',
        'kowiktionary', 'jvwiktionary', 'kmwiktionary', 'tiwiktionary', 'enwiktionary', 'gvwiktionary',
        'idwiktionary', 'sswiktionary', 'lowiktionary', 'slwiktionary', 'tkwiktionary', 'swwiktionary',
        'mywiktionary', 'zhwiktionary', 'kywiktionary', 'kkwiktionary', 'eowiktionary', 'urwiktionary',
        'jbowiktionary', 'tpiwiktionary', 'elwiktionary', 'roa_rupwiktionary', 'zh_min_nanwiktionary',
        'fjwiktionary', 'svwiktionary', 'kswiktionary', 'iuwiktionary', 'skrwiktionary', 'mnwiktionary',
        'ocwiktionary', 'bnwiktionary', 'rwwiktionary', 'huwiktionary', 'plwiktionary', 'hsbwiktionary',
        'mswiktionary', 'trwiktionary', 'hewiktionary', 'pswiktionary', 'ruwiktionary', 'zuwiktionary',
        'mkwiktionary', 'fawiktionary', 'dewiktionary', 'afwiktionary', 'lnwiktionary', 'jawiktionary',
        'arwiktionary', 'gawiktionary', 'mnwwiktionary', 'stwiktionary', 'viwiktionary', 'ptwiktionary',
        'smwiktionary', 'iawiktionary', 'aywiktionary', 'tgwiktionary', 'omwiktionary', 'klwiktionary',
        'shwiktionary', 'gdwiktionary', 'ukwiktionary', 'knwiktionary', 'sowiktionary', 'tlwiktionary',
        'glwiktionary', 'sawiktionary', 'nawiktionary', 'itwiktionary', 'mlwiktionary', 'tawiktionary',
        'tewiktionary', 'eswiktionary', 'nnwiktionary', 'cawiktionary', 'hawiktionary', 'yuewiktionary',
        'lawiktionary',
    ]
# END: initial queries against the data lake


def load_suggestions_with_feedback() -> DataFrame:  # pragma: no cover
    return spark.sql(queries.suggestions_with_feedback)


def get_link_thresholds_per_wiki(short_snapshot: str) -> DataFrame:
    wiki_sizes = load_wiki_sizes(short_snapshot)

    return (
        wiki_sizes
        .withColumn(
            'threshold',
            F.ceil(
                F.when(
                    wiki_sizes.size >= 50000,
                    (F.log10(wiki_sizes.size / 50000) + 1) * 15
                )
                .otherwise(
                    F.when(
                        wiki_sizes.size > 12500,
                        (wiki_sizes.size / 50000) * 15
                    )
                    .otherwise(4)
                )
            )
        )
    )


# If an image is used more than N times on a wiki, then it's probably a placeholder or an icon and shouldn't be
# be considered an illustration
def get_images_with_too_many_links(short_snapshot: str) -> DataFrame:
    thresholds = get_link_thresholds_per_wiki(short_snapshot)
    images = load_commons_images(short_snapshot)
    all_imagelinks = load_imagelinks(short_snapshot)
    # wiktionaries often have the same image for a word in multiple languages WITHOUT that image
    # being an icon, so don't consider the imagelink count for wiktionaries
    wiktionaries = get_wiktionaries()
    imagelinks = all_imagelinks.filter(~F.col('wiki_db').isin(wiktionaries))

    return (
        images
        .join(
            imagelinks,
            on=[images.page_title == imagelinks.image_title],
            how='inner'
        )
        .groupBy(imagelinks.wiki_db, images.page_id, images.page_title)
        .agg(
            F.count(imagelinks.article_id)
            .alias('link_count')
        )
        .join(
            thresholds,
            on=[imagelinks.wiki_db == thresholds.wiki_db],
            how='inner'
        )
        .where('link_count > threshold')
        .select('page_id', 'page_title')
    )


# Get commons images with titles that suggest they may be placeholders/icons
def get_images_with_disallowed_substrings(short_snapshot: str) -> DataFrame:
    commons_file_pages = load_commons_images(short_snapshot)
    disallowed_sql_buffer = []
    for substr in PLACEHOLDER_IMAGE_SUBSTRINGS:
        disallowed_sql_buffer.append(f"LOWER(page_title) LIKE '%{substr}%'")

    return (
        commons_file_pages
        .where(' OR '.join(disallowed_sql_buffer))
        .select('page_id', 'page_title')
    )


# Some images are not considered "illustrations" - e.g. some images are placeholders or icons
# Get all COMMONS images that *are* considered to be illustrations (we don't suggest local images)
def get_all_illustrations(short_snapshot: str) -> DataFrame:
    commons_images = load_commons_images(short_snapshot)
    images_in_placeholder_categories = load_images_in_placeholder_categories()
    images_with_too_many_links = get_images_with_too_many_links(short_snapshot)
    images_with_disallowed_substrings = get_images_with_disallowed_substrings(short_snapshot)

    return (
        commons_images
        .join(
            images_in_placeholder_categories,
            on=['page_title'],
            how='left_anti'
        )
        .join(
            images_with_too_many_links,
            on=['page_id'],
            how='left_anti'
        )
        .join(
            images_with_disallowed_substrings,
            on=['page_id'],
            how='left_anti'
        )
        .select(commons_images.page_id, commons_images.page_title)
    )


# An article counts as "unillustrated" if it either has NO images, or if any images is does have are used so widely
# that they are probably icons
def get_unillustrated_articles(short_snapshot: str) -> DataFrame:
    commons_images = load_commons_images(short_snapshot)
    icons = get_images_with_too_many_links(short_snapshot)
    commons_images_minus_icons = commons_images.join(icons, on=['page_id'], how='left_anti')

    local_images = load_local_images(short_snapshot)
    imagelinks = load_imagelinks(short_snapshot)

    illustrated_articles = (
        imagelinks
        .join(
            commons_images_minus_icons,
            on=[imagelinks.image_title == commons_images_minus_icons.page_title],
            how='inner'
        )
        .select(imagelinks.wiki_db, 'article_id')
        .union(
            imagelinks
            .join(
                local_images,
                on=[imagelinks.image_title == local_images.page_title, imagelinks.wiki_db == local_images.wiki_db ],
                how='inner'
            )
            .select(imagelinks.wiki_db, 'article_id')
        )
    )

    non_commons_main_pages = load_non_commons_main_pages(short_snapshot)

    return (
        non_commons_main_pages
        .join(
            illustrated_articles,
            on=[
                non_commons_main_pages.page_id == illustrated_articles.article_id,
                non_commons_main_pages.wiki_db == illustrated_articles.wiki_db,
            ],
            how='left_anti'
        )
        .select('wiki_db', 'page_id', 'page_title')
    )


# If an a wikidata item is an instance of a list, or a year, or a number, etc, then any article associated with it is
# not a candidate for illustration.
def get_non_illustratable_item_ids(snapshot: str) -> DataFrame:
    wikidata_items_with_P31 = load_wikidata_items_with_P31(snapshot)
    disallowed_sql_buffer = []
    for substr in UNILLUSTRATABLE_P31:
        disallowed_sql_buffer.append(f"value='{substr}'")

    return (
        wikidata_items_with_P31
        .where(' OR '.join(disallowed_sql_buffer))
        .select('item_id')
    )


def get_illustratable_articles(snapshot: str) -> DataFrame:
    short_snapshot = shared.get_monthly_snapshot(snapshot)
    unillustrated = get_unillustrated_articles(short_snapshot)
    wikidata_item_page_links = load_wikidata_item_page_links(snapshot)
    non_illustratable_item_ids = get_non_illustratable_item_ids(snapshot)

    return (
        unillustrated
        .join(
            wikidata_item_page_links,
            on=['wiki_db', 'page_id']
        )
        .join(
            non_illustratable_item_ids,
            on=[wikidata_item_page_links.item_id == non_illustratable_item_ids.item_id],
            how='left_anti',
        )
        .select('wiki_db', 'page_id', 'page_title', 'item_id')
    )


def get_illustratable_articles_with_revisions(snapshot: str) -> DataFrame:
    short_snapshot = shared.get_monthly_snapshot(snapshot)
    illustratable_articles = get_illustratable_articles(snapshot)
    latest_revisions = load_latest_revisions(short_snapshot)

    return (
        illustratable_articles
        .join(
            latest_revisions,
            on=['wiki_db', 'page_id'],
            how='inner'
        )
        .select('wiki_db', 'page_id', 'page_title', 'item_id', 'rev_id')
    )


def create_dataset_id() -> str:  # pragma: no cover
    return str(uuid.uuid1())


def prepare_depicts_data(snapshot: str) -> DataFrame:
    short_snapshot = shared.get_monthly_snapshot(snapshot)
    commons_file_pages = load_commons_images(short_snapshot)
    commons_pages_with_depicts = load_commons_pages_with_depicts(snapshot)

    return (
        commons_pages_with_depicts
        .join(
            commons_file_pages,
            on=['page_id'],
            how='inner'
        )
        .withColumn(
            shared.FOUND_ON_COLNAME, F.lit(None).cast(NullType())
        )
        .withColumn(
            KIND_COLNAME, F.lit('istype-depicts')
        )
        .withColumn(
            CONFIDENCE_COLNAME, F.lit(70)
        )
        .select('item_id', 'page_title', CONFIDENCE_COLNAME, KIND_COLNAME, shared.FOUND_ON_COLNAME)
    )


def prepare_wikidata_data(hive_db: str, snapshot: str) -> DataFrame:
    short_snapshot = shared.get_monthly_snapshot(snapshot)
    commons_file_pages = load_commons_images(short_snapshot)
    wikidata = load_wikidata_data_latest(hive_db, snapshot)

    return (
        wikidata
        .join(
            commons_file_pages,
            on=['page_id'],
            how='inner'
        )
        .withColumn(
            shared.FOUND_ON_COLNAME, F.lit(None).cast(NullType())
        )
        .withColumn(
            KIND_COLNAME,
            F.when(
                wikidata.tag == 'image.linked.from.wikidata.p18',
                F.lit('istype-wikidata-image')
            )
            .otherwise(
                F.lit('istype-commons-category')
            )
        )
        .withColumn(
            CONFIDENCE_COLNAME,
            F.when(
                wikidata.tag == 'image.linked.from.wikidata.p18',
                90
            )
            .otherwise(
                80
            )
        )
        .select('item_id', 'page_title', CONFIDENCE_COLNAME, KIND_COLNAME, shared.FOUND_ON_COLNAME)
    )


def prepare_lead_image_data(hive_db: str, snapshot: str) -> DataFrame:
    short_snapshot = shared.get_monthly_snapshot(snapshot)
    commons_file_pages = load_commons_images(short_snapshot)
    lead_images = load_lead_image_data_latest(hive_db, snapshot)

    return (
        lead_images
        .join(
            commons_file_pages,
            on=['page_id'],
            how='inner'
        )
        .withColumn(
            KIND_COLNAME, F.lit('istype-lead-image')
        )
        .withColumn(
            CONFIDENCE_COLNAME, F.lit(80)
        )
        .select('item_id', 'page_title', CONFIDENCE_COLNAME, KIND_COLNAME, shared.FOUND_ON_COLNAME)
    )


def gather_suggestions_for_items(hive_db: str, snapshot: str) -> DataFrame:
    short_snapshot = shared.get_monthly_snapshot(snapshot)
    all_data = (
        prepare_wikidata_data(hive_db, snapshot)
        .union(
            prepare_lead_image_data(hive_db, snapshot)
        )
        .union(
            prepare_depicts_data(snapshot)
        )
    )

    # Filter out suggestions that don't count as illustrations
    illustrations = get_all_illustrations(short_snapshot)
    suggestions = (
        all_data
        .join(
            illustrations,
            on=['page_title'],
            how='inner'
        )
    )
    with_conf = (
        suggestions
        .groupBy('item_id', 'page_title')
        .agg(
            F.max(CONFIDENCE_COLNAME)
            .alias(CONFIDENCE_COLNAME)
        )
    )
    # The 'orderBy' here is just to make the order predictable for automated testing purposes
    with_kind = (
        suggestions
        .orderBy(KIND_COLNAME)
        .groupBy('item_id', 'page_title')
        .agg(
            F.array_distinct(
                F.collect_list(KIND_COLNAME)
            )
            .alias(KIND_COLNAME)
        )
    )
    with_found_on = (
        suggestions
        .groupBy('item_id', 'page_title')
        .agg(
            F.first(shared.FOUND_ON_COLNAME, ignorenulls=True)
            .alias(shared.FOUND_ON_COLNAME)
        )
    )

    return (
        suggestions
        .select('item_id', 'page_title')
        .join(
            with_conf,
            on=['item_id', 'page_title'],
            how='inner'
        )
        .join(
            with_kind,
            on=['item_id', 'page_title'],
            how='inner'
        )
        .join(
            with_found_on,
            on=['item_id', 'page_title'],
            how='inner'
        )
        .distinct()
    )


def generate_suggestions(hive_db: str, snapshot: str, coalesce: int) -> DataFrame:
    dataset_id = create_dataset_id()
    suggestions = gather_suggestions_for_items(hive_db, snapshot)
    articles = get_illustratable_articles_with_revisions(snapshot)
    suggestions_with_feedback = load_suggestions_with_feedback()

    suggestions_full = (
        articles
        .join(
            suggestions,
            on=['item_id'],
            how='inner'
        )
        # exclude suggestions that have already had feedback
        .join(
            suggestions_with_feedback,
            on=[
                articles.wiki_db == suggestions_with_feedback.wiki,
                articles.page_id == suggestions_with_feedback.page_id,
                suggestions.page_title == suggestions_with_feedback.filename
            ],
            how='left_anti'
        )
        .withColumn(
            DATASET_ID_COLNAME, F.lit(dataset_id)
        )
        .withColumn(
            ORIGIN_WIKI_COLNAME, F.lit(shared.COMMONSWIKI_VALUE)
        )
        .select(
            articles.wiki_db.alias('wiki'),
            articles.page_id,
            articles.page_title,
            articles.item_id,
            DATASET_ID_COLNAME,
            suggestions.page_title.alias('image'),
            ORIGIN_WIKI_COLNAME,
            suggestions.confidence,
            suggestions.found_on,
            suggestions.kind,
            articles.rev_id.alias('page_rev')
        )
    )

    save_suggestions(suggestions_full, hive_db, snapshot, coalesce)
    save_title_cache(suggestions_full, hive_db, snapshot, coalesce)
    save_instanceof_cache(suggestions_full, hive_db, snapshot, coalesce)

    # Returning this to make it testable
    return suggestions_full


def save_suggestions(suggestions_full: DataFrame, output_db: str, snapshot: str, coalesce: int) -> DataFrame:
    suggestions = suggestions_full.withColumn('snapshot', F.lit(snapshot)).select(
        'wiki', 'page_id', DATASET_ID_COLNAME, 'image', ORIGIN_WIKI_COLNAME,
        'confidence', 'found_on', 'kind', 'page_rev', 'snapshot'
    )
    shared.save_table(
        suggestions,
        output_db, SUGGESTIONS_TABLE,
        coalesce,
        partition_columns=['snapshot', 'wiki']
    )

    # Returning this to make it testable
    return suggestions


def save_title_cache(suggestions_full: DataFrame, output_db: str, snapshot: str, coalesce: int) -> DataFrame:
    title_cache = suggestions_full.withColumn('snapshot', F.lit(snapshot)).select(
        'wiki', 'page_id', 'page_rev', suggestions_full.page_title.alias('title'), 'snapshot'
    ).distinct()
    shared.save_table(
        title_cache,
        output_db, TITLE_CACHE_TABLE,
        coalesce,
        partition_columns=['snapshot', 'wiki']
    )

    # Returning this to make it testable
    return title_cache


def save_instanceof_cache(suggestions_full: DataFrame, output_db: str, snapshot: str, coalesce: int) -> DataFrame:
    wikidata_items_with_P31 = load_wikidata_items_with_P31(snapshot)
    instanceof_cache = (
        suggestions_full
        # Left join so that we have an entry for every article even if it has no instance_of value
        .join(
            wikidata_items_with_P31,
            on=['item_id'],
            how='left'
        )
        .groupBy(
            'wiki', 'page_id', 'page_rev', 'item_id'
        )
        .agg(
            F.array_distinct(
                F.collect_list('value')
            )
            .alias('instance_of')
        )
        .withColumn('snapshot', F.lit(snapshot))
        .select('wiki', 'page_id', 'page_rev', 'instance_of', 'snapshot')
    )
    shared.save_table(
        instanceof_cache,
        output_db, INSTANCEOF_CACHE_TABLE,
        coalesce,
        partition_columns=['snapshot', 'wiki']
    )

    # Returning this to make it testable
    return instanceof_cache


def parse_args() -> argparse.Namespace:  # pragma: no cover
    parser = argparse.ArgumentParser(
        description='Gather suggested illustrations for unillustrated articles'
    )
    parser.add_argument('hive_db', help='Hive database name')
    parser.add_argument('snapshot', help='Snapshot date (YYYY-MM-DD)')
    parser.add_argument('coalesce', help='Controls the amount of files generated per Hive partition',
                        type=int, default=2)

    return parser.parse_args()


def main(args: argparse.Namespace) -> None:
    hive_db = args.hive_db
    snapshot = args.snapshot
    coalesce = args.coalesce
    generate_suggestions(hive_db, snapshot, coalesce)


if __name__ == '__main__':
    args = parse_args()

    # Prod
    spark = SparkSession.builder.getOrCreate()
    # Dev
    # from wmfdata.spark import create_custom_session  # type: ignore
    # spark = create_custom_session(
        # master='yarn',
        # app_name='image-suggestions-dev',
        # # See `modified_args` in
        # # https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/platform_eng/dags/image-suggestions_dag.py
        # spark_config={
            # 'spark.driver.cores': 2,
            # 'spark.driver.memory': '12G',
            # 'spark.executor.cores': 4,
            # 'spark.executor.memory': '9G',
            # 'spark.dynamicAllocation.enabled': 'true',
            # 'spark.dynamicAllocation.maxExecutors': '64',
            # 'spark.shuffle.service.enabled': 'true',
            # 'spark.sql.shuffle.partitions': 256,
            # 'spark.sql.sources.partitionOverwriteMode': 'dynamic',
        # }
    # )
    main(args)
    spark.stop()
