#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Health checks on most recent data written by the image suggestions pipeline"""

import json
import ssl
from urllib.request import Request, urlopen

from prometheus_client import CollectorRegistry, Gauge, push_to_gateway


def collect_hasrecommendation(metrics: dict) -> None:
    context = ssl.SSLContext(ssl.PROTOCOL_TLS)
    context.verify_mode = ssl.CERT_REQUIRED
    context.load_verify_locations(cafile='/etc/ssl/certs/ca-certificates.crt')

    search_url = 'https://api-ro.discovery.wmnet/w/index.php?search=hasrecommendation%3Aimage&title=Special%3ASearch&ns0=1&cirrusDumpResult'
    wikis = ['en', 'pt', 'id', 'ru']
    for wiki in wikis:
        request = Request(search_url)
        request.add_header('Host', '{}.wikipedia.org'.format(wiki))
        response = urlopen(request, context=context)
        data = json.load(response)
        metrics['hasrecommendation_count'].labels(wiki=wiki).set(
            data['__main__']['result']['hits']['total']['value']
        )


def register_metrics(registry: CollectorRegistry) -> dict:
    return {
        'hasrecommendation_count': Gauge(
            'image_suggestions_elasticsearch_hasrecommendation_total',
            'total number of image suggestions in elasticsearch',
            ['wiki'],
            registry=registry,
        ),
    }


def main() -> None:
    registry = CollectorRegistry()
    metrics = register_metrics(registry)
    collect_hasrecommendation(metrics)
    push_to_gateway(
        'prometheus-pushgateway.discovery.wmnet',
        job='image_suggestions_pipeline',
        registry=registry,
    )


if __name__ == '__main__':
    main()
