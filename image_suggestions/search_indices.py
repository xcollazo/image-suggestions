#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Reads the suggestions table in hive, and writes data containing flags for the weighted tags field in the
search index indicating if an article has a suggestion. See https://phabricator.wikimedia.org/T299884"""

import argparse

from fsspec import filesystem  # type: ignore
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F

from image_suggestions import queries, shared

SUGGESTIONS_TABLE = 'image_suggestions_suggestions'
MAIN_NAMESPACE_VALUE = 0
RECOMMENDATION_TAG = 'recommendation.image'
RECOMMENDATION_EXISTS_VALUE = 'exists|1'


# TODO `spark` is an outer-score variable: pass it as an arg
def load_suggestions(hive_db: str, snapshot: str) -> DataFrame:  # pragma: no cover
    return spark.sql(
        queries.suggestions.format(
            hive_db, SUGGESTIONS_TABLE, snapshot
        )
    )


def get_search_index_data(hive_db: str, snapshot: str) -> DataFrame:
    suggestions = load_suggestions(hive_db, snapshot)
    return (
        suggestions
        .withColumn(shared.PAGE_NAMESPACE_COLNAME, F.lit(MAIN_NAMESPACE_VALUE))
        .withColumn(shared.TAG_COLNAME, F.lit(RECOMMENDATION_TAG) )
        .withColumn(shared.VALUES_COLNAME, F.array(F.lit(RECOMMENDATION_EXISTS_VALUE)) )
        .select(
            suggestions.wiki.alias(shared.WIKIID_COLNAME), shared.PAGE_NAMESPACE_COLNAME, 'page_id',
            shared.TAG_COLNAME, shared.VALUES_COLNAME
        )
    )


def parse_args() -> argparse.Namespace:  # pragma: no cover
    parser = argparse.ArgumentParser(
        description='Gather weighted tags values relevant for suggested images for search indices'
    )
    parser.add_argument('suggestions_db', help='Hive database name where suggestions are stored')
    parser.add_argument('snapshot', help='Snapshot date (YYYY-MM-DD)')
    # TODO Make this a standard optional arg
    parser.add_argument('previous_snapshot', help='Previous snapshot date (YYYY-MM-DD)', nargs='?')
    parser.add_argument('coalesce', help='Controls the amount of files generated per Hive partition',
                        type=int, default=2)

    return parser.parse_args()


def main(args: argparse.Namespace) -> None:
    suggestions_db = args.suggestions_db
    snapshot = args.snapshot
    previous_snapshot = args.previous_snapshot
    coalesce = args.coalesce

    data = get_search_index_data(suggestions_db, snapshot)
    shared.write_search_index_full(data, suggestions_db, snapshot, coalesce)

    commonswiki_data = spark.sql(
        queries.search_index_commonswiki.format(
            suggestions_db, shared.SEARCH_INDEX_FULL_TABLE_NAME, snapshot
        )
    )
    shared.write_search_index_delta(
        data.union(commonswiki_data),
        suggestions_db,
        snapshot,
        previous_snapshot,
        coalesce,
        spark
    )


if __name__ == '__main__':
    args = parse_args()

    # Prod
    spark = SparkSession.builder.getOrCreate()
    # Dev
    # from wmfdata.spark import create_custom_session  # type: ignore
    # spark = create_custom_session(
        # master='yarn',
        # app_name='image-suggestions-dev',
        # # See `modified_args` in
        # # https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/platform_eng/dags/image-suggestions_dag.py
        # spark_config={
            # 'spark.driver.cores': 2,
            # 'spark.driver.memory': '12G',
            # 'spark.executor.cores': 4,
            # 'spark.executor.memory': '9G',
            # 'spark.dynamicAllocation.enabled': 'true',
            # 'spark.dynamicAllocation.maxExecutors': '64',
            # 'spark.shuffle.service.enabled': 'true',
            # 'spark.sql.shuffle.partitions': 256,
            # 'spark.sql.sources.partitionOverwriteMode': 'dynamic',
        # }
    # )
    main(args)
    spark.stop()
