#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""A set of Spark-flavoured SQL queries that gathers relevant data for image suggestions.
See https://phabricator.wikimedia.org/T299781"""


wiki_sizes = """
SELECT wiki_db,COUNT(*) as size
FROM wmf_raw.mediawiki_page
WHERE page_namespace=0
AND page_is_redirect=0
AND snapshot='{}'
GROUP BY wiki_db
"""

# String values from claims are wrapped in quotes, so we use `regexp_extract()` to remove them
# Also we need spaces replaced with underscores in image filenames to match mediawiki_page.page_title
wikidata_items_with_P18 = """SELECT we.id as item_id,
replace(regexp_extract(claim.mainSnak.datavalue.value, '^"(.*)"$', 1), ' ', '_')  as value
FROM wmf.wikidata_entity we
LATERAL VIEW OUTER explode(claims) c AS claim
WHERE typ='item'
AND claim.mainSnak.property = 'P18'
AND snapshot='{}'
"""

# String values from claims are wrapped in quotes, so we use `regexp_extract()` to remove them
# Also we need spaces replaced with underscores in category names to match category names elsewhere
wikidata_items_with_P373 = """SELECT we.id as item_id,
replace(regexp_extract(claim.mainSnak.datavalue.value, '^"(.*)"$', 1), ' ', '_')  as value
FROM wmf.wikidata_entity we
LATERAL VIEW OUTER explode(claims) c AS claim
WHERE typ='item'
AND claim.mainSnak.property = 'P373'
AND snapshot='{}'
"""

# An item-id value from a claim is stored as a json string, so need to extract it
wikidata_items_with_P31 = """SELECT we.id as item_id,
from_json(claim.mainSnak.dataValue.value, 'entityType STRING, numericId INT, id STRING').id as value
FROM wmf.wikidata_entity we
LATERAL VIEW OUTER explode(claims) c AS claim
WHERE typ='item'
AND claim.mainSnak.property = 'P31'
AND snapshot='{}'
"""

# P180 (depicts), P6243 (is digital representation of) and P912 (main subject) are all used as "depicts"
commons_pages_with_depicts = """SELECT DISTINCT
from_json(statement.mainsnak.datavalue.value, 'entityType STRING, numericId INT, id STRING').id AS item_id,
SUBSTRING( ce.id, 2 ) as page_id
FROM structured_data.commons_entity ce
LATERAL VIEW OUTER explode(statements) s AS statement
WHERE statement.mainsnak.property IN ('P180', 'P6243', 'P921')
AND snapshot='{}'
"""

commons_file_pages = """
SELECT page_id, page_title
FROM wmf_raw.mediawiki_page
WHERE page_namespace=6
AND page_is_redirect=0
AND wiki_db='commonswiki'
AND snapshot='{}'
"""

local_images = """
SELECT wiki_db, page_id, page_title
FROM wmf_raw.mediawiki_page
WHERE page_namespace=6
AND page_is_redirect=0
AND wiki_db!='commonswiki'
AND snapshot='{}'
"""

category_links = """
SELECT cl_from as page_id, cl_to as cat_title
FROM wmf_raw.mediawiki_categorylinks cl
WHERE wiki_db ='commonswiki'
AND cl_type='file'
AND snapshot='{}'
"""

categories = """
SELECT cat_title, cat_pages
FROM wmf_raw.mediawiki_category c
WHERE wiki_db='commonswiki'
AND cat_pages<100000
AND cat_pages>0
AND snapshot='{}'
"""

non_commons_main_pages = """
SELECT wiki_db,page_id,page_title
FROM wmf_raw.mediawiki_page
WHERE page_namespace=0
AND page_is_redirect=0
AND wiki_db!='commonswiki'
AND snapshot='{}'
"""

pagelinks = """
SELECT wiki_db,pl_title as to_title,pl_from as from_id
FROM wmf_raw.mediawiki_pagelinks
WHERE snapshot='{}'
"""

pages_with_lead_images = """
SELECT wiki_db,pp_page as page_id,pp_value as lead_image_title
FROM wmf_raw.mediawiki_page_props
WHERE pp_propname='page_image_free'
AND wiki_db!='commonswiki'
AND snapshot='{}'
"""

wikidata_item_page_links = """
SELECT item_id,wiki_db,page_id
FROM wmf.wikidata_item_page_link
WHERE page_namespace=0
AND snapshot='{}'
"""

imagelinks = """
SELECT wiki_db,il_from as article_id,il_to as image_title
FROM wmf_raw.mediawiki_imagelinks
WHERE il_from_namespace=0
AND wiki_db!='commonswiki'
AND snapshot='{}'
"""

latest_revisions = """
SELECT wiki_db,rev_page as page_id,MAX(rev_id) as rev_id
FROM wmf_raw.mediawiki_revision
WHERE snapshot='{}'
GROUP BY wiki_db,rev_page
"""

suggestions = """
SELECT distinct wiki,page_id FROM {}.{} WHERE snapshot='{}' ORDER BY page_id
"""

search_index = """
SELECT wikiid,page_namespace,page_id,tag,values FROM {}.{} WHERE snapshot='{}' ORDER BY page_id
"""

search_index_commonswiki = """
SELECT wikiid,page_namespace,page_id,tag,values FROM {}.{} WHERE snapshot='{}' and wikiid='commonswiki' ORDER BY page_id
"""


suggestions_with_feedback = """
SELECT wiki, page_id, filename
FROM event_sanitized.mediawiki_image_suggestions_feedback
WHERE (datacenter!='') AND year>=2022 AND month>0 AND day>0 AND hour<24
AND (is_rejected = True OR is_accepted = True)
"""