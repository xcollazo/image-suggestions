# Image suggestions
Spark jobs for the [image suggestions data pipeline](https://www.mediawiki.org/wiki/Structured_Data_Across_Wikimedia/Image_Suggestions/Data_Pipeline).

## Release workflow
We follow Data Engineering's [workflow_utils](https://gitlab.wikimedia.org/repos/data-engineering/workflow_utils/-/blob/main/gitlab_ci_templates/README.md#project-versioning):
- the `main` branch is on a `.dev` release
- releases are made by removing the `.dev` suffix and committing a tag

## How to deploy
1. On the left sidebar, go to **CI/CD > Pipelines**
2. click on the _play_ button and select `trigger_release`
3. on the left sidebar, go to **Packages and registries > Package Registry**
4. click on the first item, right-click the asset file name, and copy the URL
5. update the artifact file name in the [DAG](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/platform_eng/dags/image-suggestions_dag.py)'s `conda_env`
6. update the artifact file name and URL in the [DAG config](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/platform_eng/config/artifacts.yaml)
7. merge into `main`
8. deploy the DAGs:
```sh
me@my_box:~$ ssh deployment.eqiad.wmnet
me@deploy1002:~$ cd /srv/deployment/airflow-dags/platform_eng/
me@deploy1002:~$ git pull
me@deploy1002:~$ scap deploy
```
See the [docs](https://gitlab.wikimedia.org/repos/data-engineering/workflow_utils/-/blob/main/gitlab_ci_templates/README.md#example-pipeline-usage) for more details.
