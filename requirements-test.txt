# A boilerplate requirements file for testing
# image-suggestions 
pytest==7.2.0
pytest-spark==0.6.0
pytest-cov==4.0.0
mypy==0.991
mypy-extensions==0.4.3
typed-ast==1.5.4
pyspark-stubs==3.0.0.post3
flake8==6.0.0
pytest-mock==3.10.0
